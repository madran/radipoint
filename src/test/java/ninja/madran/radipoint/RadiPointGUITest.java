package ninja.madran.radipoint;

import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import ninja.madran.radipoint.component.point.PointDot;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.testfx.api.FxAssert;
import org.testfx.api.FxToolkit;
import org.testfx.framework.junit5.ApplicationTest;

import java.util.concurrent.TimeoutException;

public class RadiPointGUITest extends ApplicationTest {

    @BeforeAll
    static void setUp() throws Exception {

        ApplicationTest.launch(RadiPoint.class);
    }

    @Override
    public void start(Stage stage) {

        stage.show();
    }

    @AfterEach
    public void afterEach() throws TimeoutException {

        FxToolkit.hideStage();
        release(new KeyCode[]{});
        release(new MouseButton[]{});
    }

    @Test
    public void openExampleImage() {

        clickOn("File");
        clickOn("Import example image");


        FxAssert.verifyThat("#image", (ImageView imageView) -> imageView.getImage() != null);
    }

    @Test
    public void addOnePoint() {

        clickOn("File");
        clickOn("Import example image");
        clickOn(".image-pane");


        FxAssert.verifyThat(
                ".image-pane__canvas",
                (AnchorPane anchorPane) -> {

                    return 1 == anchorPane
                            .getChildren()
                            .filtered(node -> node instanceof PointDot)
                            .size();
                }
        );

        FxAssert.verifyThat(
                ".point-list",
                (VBox pointList) -> {

                    return 1 == pointList
                            .getChildren()
                            .size();
                }
        );
    }
}
