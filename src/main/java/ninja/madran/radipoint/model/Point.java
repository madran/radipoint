package ninja.madran.radipoint.model;

import javafx.beans.property.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Random;

public class Point implements Serializable {

    private static int index = 1;

    private IntegerProperty x = new SimpleIntegerProperty();

    private IntegerProperty y = new SimpleIntegerProperty();

    private ObjectProperty<Paint> color = new SimpleObjectProperty<>();

    private StringProperty name = new SimpleStringProperty();

    public Point(int x, int y) {

        this.name.setValue("Point#" + index++);
        this.x.set(x);
        this.y.set(y);
        Random random = new Random();
        color.setValue(new Color(random.nextFloat(), random.nextFloat(), random.nextFloat(), 1));
    }

    public int getX() {
        return x.get();
    }

    public IntegerProperty xProperty() {
        return x;
    }

    public int getY() {
        return y.get();
    }

    public IntegerProperty yProperty() {
        return y;
    }

    public Paint getColor() {
        return color.get();
    }

    public ObjectProperty<Paint> colorProperty() {
        return color;
    }

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    private void readObject(ObjectInputStream inputStream) throws ClassNotFoundException, IOException {

        x = new SimpleIntegerProperty(inputStream.readInt());
        y = new SimpleIntegerProperty(inputStream.readInt());
        name = new SimpleStringProperty(inputStream.readUTF());
        double blue = inputStream.readDouble();
        double red = inputStream.readDouble();
        double green = inputStream.readDouble ();
        color = new SimpleObjectProperty<>(new Color(red, green, blue, 1));
    }

    private void writeObject(ObjectOutputStream outputStream) throws IOException {

        outputStream.writeInt(x.get());
        outputStream.writeInt(y.get());
        outputStream.writeUTF(name.get());
        double blue = ((Color) color.get()).getBlue();
        double red = ((Color) color.get()).getRed();
        double green = ((Color) color.get()).getGreen();
        outputStream.writeDouble(blue);
        outputStream.writeDouble(red);
        outputStream.writeDouble(green);
    }
}
