package ninja.madran.radipoint;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import ninja.madran.radipoint.component.mainwindow.MainWindow;
import ninja.madran.radipoint.model.Point;

public class RadiPoint extends Application {

    public static void main(String[] args) {

        launch(args);
    }

    public void start(Stage primaryStage) {

        ObservableList<Point> points = FXCollections.observableArrayList();

        MainWindow mainWindow = new MainWindow(points);

        Scene scene = new Scene(mainWindow);

        primaryStage.setScene(scene);
        primaryStage.setTitle("RadiPoint");
        primaryStage.getIcons().add(new Image("img/app-icon.png"));
        primaryStage.show();
    }
}
