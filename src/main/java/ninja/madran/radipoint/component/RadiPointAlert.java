package ninja.madran.radipoint.component;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import java.util.Optional;

public class RadiPointAlert {

    private final Alert alert;

    private ButtonType pressedButtonType;

    private RadiPointAlert(Builder builder) {

        alert = new Alert(builder.type);
        alert.setTitle(builder.title);
        alert.setHeaderText(builder.header);
        alert.setContentText(builder.content);
    }

    public void show() {

        Optional<ButtonType> result = alert.showAndWait();
        pressedButtonType = result.orElse(ButtonType.CANCEL);
    }

    public boolean isAborted() {

        if (alert == null) {
            throw new IllegalStateException("You have to build RadiPointAlert object.");
        }

        return pressedButtonType == ButtonType.CANCEL || pressedButtonType == ButtonType.CLOSE;
    }

    public static class Builder {

        private String title = "";
        private String header = "";
        private String content = "";
        private Alert.AlertType type = Alert.AlertType.INFORMATION;

        public Builder() {

        }

        public Builder setTitle(String title) {

            this.title = title;
            return this;
        }

        public Builder setHeader(String header) {

            this.header = header;
            return this;
        }

        public Builder setContent(String content) {

            this.content = content;
            return this;
        }

        public Builder setType(Alert.AlertType type) {

            this.type = type;
            return this;
        }

        public RadiPointAlert build() {

            return new RadiPointAlert(this);
        }
    }
}
