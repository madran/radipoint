package ninja.madran.radipoint.component;

import javafx.stage.FileChooser;

import java.io.File;
import java.util.List;

public class RadiPointFileChooserFactory {

    public RadiPointFileChooser createRadiPointFileChooser(File initialDirectory, List<FileChooser.ExtensionFilter> filters) {

        return new RadiPointFileChooser
                .Builder()
                .setInitialDirectory(initialDirectory)
                .setExtensionFilters(filters)
                .build();
    }
}
