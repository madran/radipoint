package ninja.madran.radipoint.component.mainwindow;

import javafx.fxml.FXMLLoader;
import javafx.scene.control.SkinBase;
import javafx.scene.layout.VBox;
import ninja.madran.radipoint.component.RadiPointAlertFactory;
import ninja.madran.radipoint.component.RadiPointFileChooserFactory;

import java.io.IOException;

public class MainWindowSkin extends SkinBase<MainWindow> {

    public MainWindowSkin(MainWindow control) {

        super(control);

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/main-window.fxml"));
        loader.setControllerFactory(
                param -> new MainWindowController(
                        control,
                        new RadiPointAlertFactory(),
                        new RadiPointFileChooserFactory()
                )
        );

        try {
            VBox container = loader.load();
            getChildren().add(container);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
