package ninja.madran.radipoint.component.mainwindow;

import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import ninja.madran.radipoint.component.*;
import ninja.madran.radipoint.component.imagegrid.ImageGrid;
import ninja.madran.radipoint.component.point.PointInput;
import ninja.madran.radipoint.model.Point;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.util.*;

public class MainWindowController implements Initializable {

    private ImageGrid imageGrid;

    private final ObservableList<Point> points;

    private final RadiPointAlertFactory alertFactory;

    private final RadiPointFileChooserFactory fileChooserFactory;

    @FXML
    private VBox pointList;

    @FXML
    HBox content;

    private File currentLoadedImageFile;

    public MainWindowController(MainWindow control, RadiPointAlertFactory alertFactory, RadiPointFileChooserFactory fileChooserFactory) {

        points = control.getPoints();
        this.alertFactory = alertFactory;
        this.fileChooserFactory = fileChooserFactory;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        imageGrid = new ImageGrid(points);
        HBox.setHgrow(imageGrid, Priority.ALWAYS);
        content.getChildren().add(imageGrid);

        points.addListener((ListChangeListener<Point>) c -> {
            while (c.next()) {
                if (c.wasAdded()) {
                    for (Point point : c.getAddedSubList()) {
                        PointInput pointInput = new PointInput(
                                point,
                                new ImageSize(
                                        (int) imageGrid.getImage().getWidth(),
                                        (int) imageGrid.getImage().getHeight()
                                )
                        );

                        pointInput.colorProperty().bindBidirectional(point.colorProperty());
                        pointInput.xProperty().bindBidirectional(point.xProperty());
                        pointInput.yProperty().bindBidirectional(point.yProperty());
                        pointInput.nameProperty().bindBidirectional(point.nameProperty());
                        pointList.getChildren().add(pointInput);
                    }
                }

                if (c.wasRemoved()) {
                    for (Point p : c.getRemoved()) {
                        FilteredList<Node> filtered = pointList.getChildren().filtered(
                                node -> {
                                    if (node instanceof PointInput) {
                                        PointInput pointInput = (PointInput) node;
                                        return pointInput.getPoint() == p;
                                    }

                                    return false;
                                }
                        );
                        pointList.getChildren().removeAll(filtered);
                    }
                }
            }
        });
    }

    public void importImage(ActionEvent actionEvent) {

        currentLoadedImageFile = loadImageFile();

        if (currentLoadedImageFile != null) {
            if (!points.isEmpty()) {

                if (isLoosingPointsConfirmedAlert()) {
                    return;
                }
            }

            points.clear();

            imageGrid.setImage(new Image(currentLoadedImageFile.toURI().toString()));
        }
    }

    public void importExampleImage(ActionEvent actionEvent) {

        if (!points.isEmpty()) {
            if (isLoosingPointsConfirmedAlert()) {
                return;
            }
        }

        currentLoadedImageFile = new File("/img/java.png");
        imageGrid.setImage(new Image("/img/java.png"));
        points.clear();
    }

    public void saveProject(ActionEvent actionEvent) throws IOException {

        Image image = imageGrid.getImage();

        if (image == null) {
            noImageLoadedWarningAlert();
            return;
        }

        BufferedImage bImage = SwingFXUtils.fromFXImage(image, null);
        ByteArrayOutputStream s = new ByteArrayOutputStream();

        String type = Files.probeContentType(currentLoadedImageFile.toPath());
        String[] typeParts = type.split("/");
        String extension = typeParts[typeParts.length - 1];

        ImageIO.write(bImage, extension, s);

        byte[] res = s.toByteArray();

        String encodedString = Base64.getEncoder().encodeToString(res);


        List<Point> pointList = new ArrayList<>(points);
        List<Object> data = Arrays.asList(encodedString, pointList);

        File file = saveRpiFile();

        if (file == null) {
            return;
        }

        try (
                FileOutputStream fileStream = new FileOutputStream(file);
                ObjectOutputStream serializedData = new ObjectOutputStream(fileStream)
        ) {
            serializedData.writeObject(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void loadProject(ActionEvent actionEvent) {

        File selectedFile = loadRpiFile();

        if (selectedFile == null) {
            return;
        }

        if (!points.isEmpty()) {
            if (isLoosingPointsConfirmedAlert()) {
                return;
            }
        }

        try (
                FileInputStream fileIn = new FileInputStream(selectedFile);
                ObjectInputStream in = new ObjectInputStream(fileIn)
        ) {
            List<Object> a = (List<Object>) in.readObject();
            byte[] decodedBytes = Base64.getDecoder().decode((String) a.get(0));
            ByteArrayInputStream bdd = new ByteArrayInputStream(decodedBytes);
            Image img = new Image(bdd);

            imageGrid.setImage(img);

            List<Point> plist = (List<Point>) a.get(1);

            points.setAll(plist);

        } catch (Exception e) {
            invalidFileWarningAlert();
        }

    }

    private void invalidFileWarningAlert() {
        RadiPointAlert alert = alertFactory.createAlert(
                Alert.AlertType.WARNING,
                "Warning",
                "Invalid file.",
                "This is not RadiPoint Image file."
        );

        alert.show();
    }

    private void noImageLoadedWarningAlert() {
        RadiPointAlert alert = alertFactory.createAlert(
                Alert.AlertType.WARNING,
                "Warning",
                "No image was loaded.",
                "You can not export project without image."
        );

        alert.show();
    }

    private boolean isLoosingPointsConfirmedAlert() {
        RadiPointAlert alert = alertFactory.createAlert(
                Alert.AlertType.CONFIRMATION,
                "Confirmation",
                "You will loose added points.",
                "Are you ok with this?"
        );

        alert.show();

        return alert.isAborted();
    }

    private File loadImageFile() {

        List<FileChooser.ExtensionFilter> filters = new ArrayList<>();
        filters.add(new FileChooser.ExtensionFilter("JPG", "*.jpg"));
        filters.add(new FileChooser.ExtensionFilter("PNG", "*.png"));

        RadiPointFileChooser fileChooser = fileChooserFactory.createRadiPointFileChooser(
                new File(System.getProperty("user.home")),
                filters
        );

        return fileChooser.openFile(content.getScene().getWindow());
    }

    private File loadRpiFile() {

        List<FileChooser.ExtensionFilter> filters = new ArrayList<>();
        filters.add(new FileChooser.ExtensionFilter("RadiPoint Image", "*.rpi"));

        RadiPointFileChooser fileChooser = fileChooserFactory.createRadiPointFileChooser(
                new File(System.getProperty("user.home")),
                filters
        );

        return fileChooser.openFile(content.getScene().getWindow());
    }

    private File saveRpiFile() {

        List<FileChooser.ExtensionFilter> filters = new ArrayList<>();
        filters.add(new FileChooser.ExtensionFilter("RadiPoint Image", "*.rpi"));

        RadiPointFileChooser fileChooser = fileChooserFactory.createRadiPointFileChooser(
                new File(System.getProperty("user.home")),
                filters
        );

        return fileChooser.saveFile(content.getScene().getWindow());
    }
}
