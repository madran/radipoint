package ninja.madran.radipoint.component.mainwindow;

import javafx.collections.ObservableList;
import javafx.scene.control.Control;
import javafx.scene.control.Skin;
import ninja.madran.radipoint.component.imagegrid.ImageGrid;
import ninja.madran.radipoint.model.Point;

public class MainWindow extends Control {

    ObservableList<Point> points;

    public MainWindow(ObservableList<Point> points) {

        this.points = points;
    }

    public ObservableList<Point> getPoints() {

        return points;
    }

    @Override
    public String getUserAgentStylesheet() {

        return ImageGrid.class.getResource("/css/main-window.css").toExternalForm();
    }

    @Override
    protected Skin<?> createDefaultSkin() {

        return new MainWindowSkin(this);
    }
}
