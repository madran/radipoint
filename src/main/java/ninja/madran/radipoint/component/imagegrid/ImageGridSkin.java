package ninja.madran.radipoint.component.imagegrid;

import javafx.fxml.FXMLLoader;
import javafx.scene.control.SkinBase;
import javafx.scene.layout.GridPane;

import java.io.IOException;

public class ImageGridSkin extends SkinBase<ImageGrid> {

    public ImageGridSkin(ImageGrid control) {

        super(control);

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/image-grid.fxml"));
        loader.setControllerFactory(param -> new ImageGridController(control));

        try {
            GridPane container = loader.load();
            getChildren().add(container);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
