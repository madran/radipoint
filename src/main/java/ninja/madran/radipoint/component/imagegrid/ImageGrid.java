package ninja.madran.radipoint.component.imagegrid;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import javafx.scene.control.Control;
import javafx.scene.control.Skin;
import javafx.scene.image.Image;
import ninja.madran.radipoint.model.Point;

public class ImageGrid extends Control {

    private final ObjectProperty<Image> image = new SimpleObjectProperty<>();

    private final ObservableList<Point> points;

    public ImageGrid(ObservableList<Point> points) {

        this.points = points;
    }

    public ObjectProperty<Image> imageProperty() {

        return image;
    }

    public void setImage(Image image) {

        this.image.setValue(image);
    }

    public Image getImage() {

        return image.getValue();
    }

    public ObservableList<Point> getPoints() {
        
        return points;
    }

    @Override
    public String getUserAgentStylesheet() {

        return ImageGrid.class.getResource("/css/image-grid.css").toExternalForm();
    }

    @Override
    protected Skin<?> createDefaultSkin() {

        return new ImageGridSkin(this);
    }
}
