package ninja.madran.radipoint.component.imagegrid;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.GridPane;
import ninja.madran.radipoint.component.imagepane.ImagePane;
import ninja.madran.radipoint.model.Point;

import java.net.URL;
import java.util.ResourceBundle;

public class ImageGridController implements Initializable {

    @FXML
    private GridPane imageGrid;

    private final ImageGrid control;

    private final ObservableList<Point> points;

    public ImageGridController(ImageGrid control) {

        this.control = control;
        this.points = control.getPoints();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        imageGrid.add(buildImagePane(), 0, 0);
        imageGrid.add(buildImagePane(), 1, 0);
        imageGrid.add(buildImagePane(), 0, 1);
        imageGrid.add(buildImagePane(), 1, 1);
    }

    private ImagePane buildImagePane() {

        ImagePane imagePane = new ImagePane(points, control.imageProperty());
        imagePane.setOnImageClick(event -> points.add(new Point((int) event.getX(), (int) event.getY())));

        return imagePane;
    }
}
