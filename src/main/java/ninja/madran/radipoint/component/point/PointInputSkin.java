package ninja.madran.radipoint.component.point;

import javafx.fxml.FXMLLoader;
import javafx.scene.control.SkinBase;
import javafx.scene.layout.HBox;

import java.io.IOException;

public class PointInputSkin extends SkinBase<PointInput> {

    public PointInputSkin(PointInput control) {

        super(control);

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/view/point-input.fxml"));
        fxmlLoader.setControllerFactory(param -> new PointInputController(control));

        try {
            HBox container = fxmlLoader.load();
            getChildren().add(container);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
