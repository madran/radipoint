package ninja.madran.radipoint.component.point;

import javafx.beans.property.*;
import javafx.scene.control.Control;
import javafx.scene.control.Skin;
import javafx.scene.paint.Paint;
import ninja.madran.radipoint.component.ImageSize;
import ninja.madran.radipoint.model.Point;

public class PointInput extends Control {

    private final Point point;

    private final StringProperty name = new SimpleStringProperty();

    private final IntegerProperty x = new SimpleIntegerProperty();

    private final IntegerProperty y = new SimpleIntegerProperty();

    private final ObjectProperty<Paint> color = new SimpleObjectProperty<>();

    private final ImageSize imageSize;

    public PointInput(Point point, ImageSize imageSize) {

        this.point = point;
        this.imageSize = imageSize;
    }

    public Point getPoint() {

        return point;
    }

    public ImageSize getImageSize() {

        return imageSize;
    }

    public StringProperty nameProperty() {

        return name;
    }

    public IntegerProperty xProperty() {

        return x;
    }

    public IntegerProperty yProperty() {

        return y;
    }

    public ObjectProperty<Paint> colorProperty() {

        return color;
    }

    @Override
    public String getUserAgentStylesheet() {

        return PointInput.class.getResource("/css/point-input.css").toExternalForm();
    }

    @Override
    protected Skin<?> createDefaultSkin() {

        return new PointInputSkin(this);
    }
}
