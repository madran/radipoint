package ninja.madran.radipoint.component.point;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import ninja.madran.radipoint.model.Point;

public class PointDot extends Circle {

    private final Point point;

    public PointDot(Point point) {

        this.point = point;

        setRadius(5);
        setStroke(Color.BLACK);
        setFill(Color.WHITE);
    }

    public Point getPoint() {

        return point;
    }
}
