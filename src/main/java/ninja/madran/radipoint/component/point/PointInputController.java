package ninja.madran.radipoint.component.point;

import javafx.beans.property.IntegerProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Rectangle;
import javafx.util.converter.NumberStringConverter;

import java.net.URL;
import java.util.ResourceBundle;

public class PointInputController implements Initializable {

    private final PointInput control;

    @FXML
    private ColorPicker colorPicker;

    @FXML
    private TextField name;

    @FXML
    private TextField x;

    @FXML
    private TextField y;

    @FXML
    private Rectangle color;

    private final int maxX;
    private final int maxY;

    public PointInputController(PointInput control) {

        this.control = control;
        maxX = control.getImageSize().getX();
        maxY = control.getImageSize().getY();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        colorPicker.setOnAction(event -> color.fillProperty().setValue(colorPicker.getValue()));
        name.textProperty().bindBidirectional(control.nameProperty());
        initTextField(x, control.xProperty(), maxX);
        initTextField(y, control.yProperty(), maxY);
        color.fillProperty().bindBidirectional(control.colorProperty());
    }

    private void initTextField(TextField textField, IntegerProperty integerProperty, int max) {

        textField.textProperty().bindBidirectional(integerProperty, new NumberStringConverter("#"));
        textField.textProperty().addListener((observable, oldValue, newValue) -> {

            if (newValue.isEmpty()) {
                textField.textProperty().setValue("0");
            } else if (Integer.parseInt(newValue) > max) {
                integerProperty.setValue(max);
            }
        });
        textField.setTextFormatter(inputCordFormatter());
    }

    @FXML
    public void showColorPicker(MouseEvent mouseEvent) {

        colorPicker.show();
    }

    @FXML
    private void selectInputText(MouseEvent e) {

        TextField input = (TextField) e.getSource();
        input.selectAll();
    }

    private TextFormatter<String> inputCordFormatter() {

        return new TextFormatter<>(change -> {

            if (change.getText().matches("^[0-9]*$")) {
                return change;
            } else {
                return null;
            }
        });
    }
}
