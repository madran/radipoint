package ninja.madran.radipoint.component;

import javafx.stage.FileChooser;
import javafx.stage.Window;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class RadiPointFileChooser {

    FileChooser fileChooser;

    private RadiPointFileChooser(Builder builder) {

        fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(builder.initialDirectory);
        fileChooser.getExtensionFilters().setAll(builder.filters);
    }

    public File openFile(Window window) {

        return fileChooser.showOpenDialog(window);
    }

    public File saveFile(Window window) {

        return fileChooser.showSaveDialog(window);
    }

    public static class Builder {

        private List<FileChooser.ExtensionFilter> filters = new ArrayList<>();

        private File initialDirectory;

        public Builder addExtensionFilter(FileChooser.ExtensionFilter filter) {

            filters.add(filter);
            return this;
        }

        public Builder setExtensionFilters(List<FileChooser.ExtensionFilter> filters) {

            this.filters = filters;
            return this;
        }

        public Builder setInitialDirectory(File initialDirectory) {

            this.initialDirectory = initialDirectory;
            return this;
        }

        public RadiPointFileChooser build() {

            return new RadiPointFileChooser(this);
        }
    }
}
