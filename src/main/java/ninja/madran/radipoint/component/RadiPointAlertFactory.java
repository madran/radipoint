package ninja.madran.radipoint.component;

import javafx.scene.control.Alert;

public class RadiPointAlertFactory {

    public RadiPointAlert createAlert(Alert.AlertType type, String title, String header, String content) {

        return new RadiPointAlert.Builder()
                .setType(type)
                .setTitle(title)
                .setHeader(header)
                .setContent(content)
                .build();
    }
}
