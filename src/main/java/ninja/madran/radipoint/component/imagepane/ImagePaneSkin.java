package ninja.madran.radipoint.component.imagepane;

import javafx.fxml.FXMLLoader;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SkinBase;

import java.io.IOException;

public class ImagePaneSkin extends SkinBase<ImagePane> {

    protected ImagePaneSkin(ImagePane control) {

        super(control);

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/image-pane.fxml"));
        loader.setControllerFactory( param -> new ImagePaneController(control));

        try {
            ScrollPane container = loader.load();
            getChildren().add(container);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
