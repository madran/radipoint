package ninja.madran.radipoint.component.imagepane;

import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import ninja.madran.radipoint.component.point.PointDot;
import ninja.madran.radipoint.model.Point;

import java.net.URL;
import java.util.ResourceBundle;

public class ImagePaneController implements Initializable {

    private final ImagePane control;

    @FXML
    private ImageView image;

    @FXML
    private AnchorPane canvas;

    @FXML
    private StackPane canvasContainer;

    private final ObservableList<Point> points;

    public ImagePaneController(ImagePane control) {

        this.control = control;
        points = control.getPoints();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        image.imageProperty().bindBidirectional(control.imageProperty());

        canvas.setOnMousePressed(control.getOnImageClick());

        control.imageProperty().addListener(this::setupCanvasSize);

        points.addListener((ListChangeListener<Point>) c -> {
            while (c.next()) {
                if (c.wasAdded()) {
                    for (Point point : c.getAddedSubList()) {
                        addPointDot(point);
                    }
                }

                if (c.wasRemoved()) {
                    for (Point point : c.getRemoved()) {
                        removeDotPoint(point);
                    }
                }
            }
        });
    }

    private void addPointDot(Point point) {
        PointDot pointDot = new PointDot(point);
        pointDot.fillProperty().bindBidirectional(point.colorProperty());
        pointDot.layoutXProperty().bindBidirectional(point.xProperty());
        pointDot.layoutYProperty().bindBidirectional(point.yProperty());

        pointDot.setOnMouseClicked(this::removePoint);
        pointDot.setOnMousePressed(Event::consume);
        pointDot.setOnMouseDragged(event -> {
            if ((event.getX() + point.getX()) > image.getImage().getWidth()) {
                pointDot.layoutXProperty().setValue(image.getImage().getWidth());
            } else if ((event.getX() + point.getX()) < 0) {
                pointDot.layoutXProperty().setValue(0);
            } else {
                pointDot.layoutXProperty().setValue(event.getX() + point.getX());
            }

            if ((event.getY() + point.getY()) > image.getImage().getHeight()) {
                pointDot.layoutYProperty().setValue(image.getImage().getHeight());
            } else if ((event.getY() + point.getY()) < 0) {
                pointDot.layoutYProperty().setValue(0);
            } else {
                pointDot.layoutYProperty().setValue(event.getY() + point.getY());
            }

        });

        canvas.getChildren().add(pointDot);

        addTooltip(point, pointDot);
    }

    private void removeDotPoint(Point point) {

        canvas.getChildren().removeIf(
                node -> {
                    if (node instanceof PointDot) {
                        PointDot pointDot = (PointDot) node;
                        return pointDot.getPoint() == point;
                    }

                    return false;
                }
        );
    }

    private void removePoint(MouseEvent mouseEvent) {

        if (mouseEvent.getButton().equals(MouseButton.PRIMARY)) {
            if (mouseEvent.getClickCount() == 2) {
                PointDot pointDot = (PointDot) mouseEvent.getSource();
                points.removeIf(point -> point.equals(pointDot.getPoint()));
            }
        }
        mouseEvent.consume();
    }

    private void setupCanvasSize(ObservableValue<? extends Image> observable, Image oldValue, Image newValue) {

        canvas.maxWidthProperty().bind(image.getImage().widthProperty());
        canvas.maxHeightProperty().bind(image.getImage().heightProperty());
        canvasContainer.minWidthProperty().bind(image.getImage().widthProperty());
        canvasContainer.minHeightProperty().bind(image.getImage().heightProperty());
    }

    private void addTooltip(Point point, PointDot pointDot) {
        Tooltip tooltip = new Tooltip();
        tooltip.setText(point.getName());
        Tooltip.install(pointDot, tooltip);
        point.nameProperty().addListener((observable, oldValue, newValue) -> tooltip.setText(newValue));
    }
}
