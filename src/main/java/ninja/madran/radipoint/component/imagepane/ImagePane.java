package ninja.madran.radipoint.component.imagepane;

import javafx.beans.property.ObjectProperty;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.control.Control;
import javafx.scene.control.Skin;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import ninja.madran.radipoint.model.Point;

public class ImagePane extends Control {

    private final ObservableList<Point> points;

    ObjectProperty<Image> image;

    EventHandler<MouseEvent> imageClickListener;

    public ImagePane(ObservableList<Point> points, ObjectProperty<Image> image) {

        this.points = points;
        this.image = image;
    }

    public ObjectProperty<Image> imageProperty() {

        return image;
    }

    public ObservableList<Point> getPoints() {

        return points;
    }

    public void setOnImageClick(EventHandler<MouseEvent> listener) {

        imageClickListener = listener;
    }

    EventHandler<MouseEvent> getOnImageClick() {

        return imageClickListener;
    }

    @Override
    public String getUserAgentStylesheet() {
        return getClass().getResource("/css/image-pane.css").toExternalForm();
    }

    @Override
    protected Skin<?> createDefaultSkin() {

        return new ImagePaneSkin(this);
    }
}
